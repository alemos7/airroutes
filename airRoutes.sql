-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 11-10-2021 a las 16:20:52
-- Versión del servidor: 8.0.26-0ubuntu0.20.04.3
-- Versión de PHP: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `airRoutes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `flights`
--

CREATE TABLE `flights` (
  `id` int NOT NULL,
  `code` varchar(45) NOT NULL,
  `origin` varchar(45) NOT NULL,
  `destiny` varchar(45) NOT NULL,
  `departure_date` timestamp NOT NULL,
  `capacity` int DEFAULT '0',
  `cost` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4  ;

--
-- Volcado de datos para la tabla `flights`
--

INSERT INTO `flights` (`id`, `code`, `origin`, `destiny`, `departure_date`, `capacity`, `cost`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 'COD-002', 'COR', 'VEN', '2021-10-10 03:00:00', 54, 400.00, '2021-10-11 14:25:52', NULL, NULL),
(8, 'COD-003', 'VEN', 'EZE', '2021-10-10 03:00:00', 54, 400.00, '2021-10-11 14:25:53', NULL, NULL),
(9, 'COD-004', 'COR', 'EZE', '2021-10-10 03:00:00', 54, 400.00, '2021-10-11 14:25:54', NULL, NULL),
(10, 'COD-005', 'COR', 'EZE', '2021-10-10 03:00:00', 54, 400.00, '2021-10-11 14:25:54', NULL, NULL),
(11, 'COD-006', 'COR', 'EZE', '2021-10-10 03:00:00', 54, 400.00, '2021-10-11 14:25:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `id` int NOT NULL,
  `id_flight` int NOT NULL,
  `code_ticket` varchar(45) NOT NULL,
  `quantity` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4  ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `flights`
--
ALTER TABLE `flights`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_flights` (`id_flight`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `flights`
--
ALTER TABLE `flights`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `fk_flights` FOREIGN KEY (`id_flight`) REFERENCES `flights` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
