
%dw 2.0
output application/java 
---
{
    id : 67,
    code : "sunt",
    origin : "eos",
    destiny : "voluptate",
    departure_date : |2015-11-12|,
    capacity : 66,
    cost : 42,
    created_at : |2003-10-06|,
    updated_at : |2005-11-23|,
    deleted_at : |2003-07-03|
}
