
%dw 2.0
output application/json
---
{
    id : 42,
    code : "COD-002",
    origin : "COR",
    destiny : "VEN",
   departure_date : |2011-01-10|,
    capacity : 50,
    cost : 7157.65,
    created_at : |2014-05-19|,
    updated_at : |2019-08-13|,
    deleted_at : |2013-10-19|,
  }
