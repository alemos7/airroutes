
%dw 2.0
output application/java 
---
[{
    id : 35,
    code : "enim",
    origin : "ut",
    destiny : "ut",
    departure_date : |2013-11-19|,
    capacity : 89,
    cost : 49,
    created_at : |2018-01-08|,
    updated_at : |2008-05-08|,
    deleted_at : |2002-06-24|,
  },
{
    id : 59,
    code : "irure",
    origin : "modi",
    destiny : "quia",
    departure_date : |2008-04-22|,
    capacity : 14,
    cost : 3,
    created_at : |2015-03-18|,
    updated_at : |2016-02-11|,
    deleted_at : |2004-07-21|,
  }]
