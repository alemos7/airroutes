
%dw 2.0
output application/java 
---
[{
    id : 97,
    code : "COD-0001",
    origin : "EZE",
    destiny : "COR",
    departure_date : |2011-01-10|,
    capacity : 45,
    cost : 15000.65,
    created_at : |2019-01-03|,
    updated_at : |2014-05-15|,
    deleted_at : |2018-02-08|,
  },
{
    id : 23,
    code : "COD-0023",
    origin : "VEN",
    destiny : "ARG",
    departure_date : |2018-08-23|,
    capacity : 189,
    cost : 85015,98,
    created_at : |2017-03-07|,
    updated_at : |2007-11-14|,
    deleted_at : |2017-06-04|,
  }]
