**Exercise Approach:**

Considering the previous interview, it is necessary to develop a service that allows the information of the different existing flights to be made available. The security, availability and scalability of the service must be considered, if possible contemplate the acquisition of tickets and visualize the availability for each flight.

Acceptance criteria: Have four endpoints (Show all flights, show a particular flight, insert flights, update flights, delete flights and have a flight search engine)

**Proposed Solution:**

An application is developed using the Mulesoft 4 platform and the Anypoint Studio 7 ide, additionally the API design and its contract to be consumed by third parties are built on the platform, the necessary tests are built and there is a coverage of 40 % in them.

The solution has six endpoints: Create, Update, Search by an element identifier, Search by origin and destination parameters, Update and Delete flights.
